/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class Game {

    private Player player1, player2;
    private Board board = new Board(player1, player2);
    private String continute;
    private boolean isFinish = false;
    private String[][] b = board.getBoard();

    public Game() {
        player1 = new Player("x");
        player2 = new Player("o");
        board = new Board(player1, player2);
    }

    public void play() {
        printWelcome();
        newGame();
        while (!isFinish) {
            b = board.getBoard();
            printTable();
            printTurn();
            inputRowCol();
            if (board.PlayerWin()) {
                printTable();
                showWinner();
                printPlayers();
                PContinute();
                board.resetTable();
                b = board.getBoard();
            }
            if (board.PlayerDraw()) {
                printTable();
                printDraw();
                printPlayers();
                PContinute();
                board.resetTable();
                b = board.getBoard();
            }
            board.nextTurn();

        }

    }

    private void printWelcome() {
        System.out.println("Welcome to XO Game!");
    }

    private void printTable() {
        String[][] b = board.getBoard();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("  " + b[i][j] + " ");
            }
            System.out.println();
        }

    }

    private void printTurn() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please in put row and column: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        if (board.setRowCol(row, col)) {

        }

    }

    private void newGame() {
        board = new Board(player1, player2);
    }

    private void showWinner() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " Win!!");
    }

    private void printDraw() {
        System.out.println("Draw!");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void PContinute() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to continute (Y/N) : ");
        continute = sc.next();
        while (!continute.toLowerCase().equals("n") && !continute.toLowerCase().equals("y")) {
            System.out.print("Please tell me if you want to continue or not!!! (Y/N) : ");
            continute = sc.next().toLowerCase();

        }
        if (continute.toLowerCase().equals("n")) {
            isFinish = true;
        }
    }

}
