/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author user
 */
public class Board {

    private String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
    private Player player1, player2, currentPlayer;
    private int row, col;

    public Board(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public String[][] getBoard() {
        return board;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean setRowCol(int row, int col) {
        this.row = row;
        this.col = col;
        if (((row < 4 && row > 0) && (col > 0 && col < 4))) {
            if (board[row - 1][col - 1].equals("_")) {
                board[row - 1][col - 1] = currentPlayer.getSymbol();
                return true;
            } else {
                System.out.println("Plese input again");
                return false;
            }
        } else {
            System.out.println("Plese input again");
            return false;
        }

    }

    private boolean checkRow() {
        for (int j = 0; j < board[row - 1].length; j++) {
            if (!board[row - 1][j].toLowerCase().equals(currentPlayer.getSymbol())) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol() {
        for (int j = 0; j < board[0].length; j++) {
            if (board[0][j].toLowerCase().equals(currentPlayer.getSymbol()) && board[1][j].toLowerCase().equals(currentPlayer.getSymbol()) && board[2][j].toLowerCase().equals(currentPlayer.getSymbol())) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagonal() {
        if (board[0][0].toLowerCase().equals(currentPlayer.getSymbol()) && board[1][1].toLowerCase().equals(currentPlayer.getSymbol()) && board[2][2].toLowerCase().equals(currentPlayer.getSymbol())) {
            return true;
        }

        if (board[0][2].toLowerCase().equals(currentPlayer.getSymbol()) && board[1][1].toLowerCase().equals(currentPlayer.getSymbol()) && board[2][0].toLowerCase().equals(currentPlayer.getSymbol())) {
            return true;
        }

        return false;
    }

    public boolean PlayerWin() {
        if (checkRow() || checkCol() || checkDiagonal()) {
            saveWin();
            return true;
        }
        return false;
    }

    private void saveWin() {
        if (currentPlayer == player1) {
            player1.win();
            player2.lose();
        } else {
            player2.win();
            player1.lose();
        }

    }

    public boolean PlayerDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j].equals("_")) {
                    return false;
                }
            }
        }
        player1.draw();
        player2.draw();
        return true;

    }

    public void nextTurn() {
        if (currentPlayer.equals(player1)) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }
    public void resetTable() {
        String[][] newboard = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
        setBoard(newboard);
    }

}
